Video Metadata REST API - more monolith approach
================================================

**Description**

Simple REST project utilising more monolith way of building applications - mainly just to show different - more complicated Maven multi module project setup.

Authentication is based on Spring Security with JWT. Available test users:
 
    user::user, admin::admin, super::super.
    
Application extracts video meta data calling MediaInfo library.

**Compiling**

    $mvn clean install

**Configuration**

    -Dspring.profiles.active="h2|hsqldb|mysql|oracle|swagger|heroku"

**Running**

Module rest/rest-config contains fat jar in target folder

    $java -jar <package_name>

available parameters (h2 together with swagger are the default ones):

    -Dserver.port=8088
    -Dserver.address=123.456.789.12
    -Dspring.profiles.active="h2|hsqldb|mysql|oracle|swagger|heroku"   
    
There is exposed h2 console for the development purposes - the link is as below and the url as following: jdbc:h2:mem:test. Link to h2 console is currently outside the security rules.
    
    http://localhost:8080/h2-console
    
**Heroku**

Application is deployed to Heroku cloud and available under the following link:

    https://video-meta-data.herokuapp.com
                                
    GET /login (raw: {"username":"xxxxx","password":"yyyyy"} )
    POST /api/video-meta/videos (name=file) 
    GET /api/video-meta/videos/{id} (test data available for id=999)

more on:
    
    https://video-meta-data.herokuapp.com/swagger-ui.html#/
    
Please be aware that Heroku stops not used processes - so each first request after idle time may need some time to be processed.
    
**Other server**

    Application is also deployed to http://80.238.106.187:8181 server
     
**Tests**

Get unit tests run

    mvn test 
    
Get unit tests and integration tests run (supported only for Admin & CSR as they have proper unit and integration tests separation)

    mvn integration-test 

or 
    
    mvn verify
    
To check the coverage report from both unit and integration tests execution
 
    mvn clean cobertura:cobertura-integration-test

**API online documentation**

Run Platform REST API with swagger profile and go to the below URL. Link to REST API documentation is currently outside the security rules.

    http://localhost:8080/swagger-ui.html#/
   
    