package com.example.persistence.model;

import lombok.Data;

import javax.persistence.*;
import java.sql.Blob;

/**
 * Created by mjzi on 2017-07-15
 */
@Entity
@Data
public class VideoDataBlob extends AbstractEntity {

	public VideoDataBlob() {
	}

	private Blob data;

	@Lob
	@Column(name = "data", length = Integer.MAX_VALUE)
	@Basic(fetch = FetchType.LAZY)
	public Blob getData() {
		return data;
	}

	public void setData(Blob data) {
		this.data = data;
	}
}
