package com.example.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

/**
 * Created by mjzi on 2017-07-15
 */
@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Video extends AbstractEntity {

	//TODO: Strings?
	private String duration;
	private String size;
	private String videoBitRate;
	private String videoCodec;
	private String audioBitRate;
	private String audioCodec;

	private String name;
}
