insert into user(id, username, password, enabled) values(1, 'user', 'user', 1);
insert into user(id, username, password, enabled) values(2, 'admin', 'admin', 1);
insert into user(id, username, password, enabled) values(3, 'super', 'super', 1);

insert into user_roles(user_role_id, role, user_id) values(1, 'ROLE_USER', 1);
insert into user_roles(user_role_id, role, user_id) values(2, 'ROLE_ADMIN', 2);
insert into user_roles(user_role_id, role, user_id) values(3, 'ROLE_USER', 3);
insert into user_roles(user_role_id, role, user_id) values(4, 'ROLE_ADMIN', 3);

insert into video(id, duration, size, video_bit_rate, video_codec, audio_bit_rate, audio_codec, name) values (999, '1800', '1901822', '7711741', 'AVC', '705600', 'PCM', 'Movie.MOV');
