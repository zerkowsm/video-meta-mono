package com.example.video.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

/**
 * Created by mjzi on 2017-07-15
 */
@Configuration
@Profile("swagger")
@EnableSwagger2
public class SwaggerConfiguration {

	@Bean
	@Profile("swagger")
	public Docket videoApi() {
		return new Docket(SWAGGER_2)
			.groupName("VIDEO-API")
			.select()
			.apis(RequestHandlerSelectors.basePackage("com.example.video.rest.api.video"))
			.paths(PathSelectors.any())
			.build().apiInfo(apiInfo());
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
			.title("Video REST API Documentation")
			.description("Video REST API Documentation")
			.version("0.0.1")
			.build();
	}
}