package com.example.video.rest.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * Created by mjzi on 2017-07-15
 */
@Configuration
@ConfigurationProperties(prefix = "spring.datasource.example")
@EntityScan(basePackages = {PersistenceConfiguration.PERSISTENCE_MODEL_PACKAGE})
@EnableJpaRepositories(basePackages = {"com.example.video.repository"})
public class PersistenceConfiguration extends HikariConfig {

	public static final String PERSISTENCE_MODEL_PACKAGE = "com.example.persistence.model";

	@Getter
	@Setter
	private String dataSourceClassName;

	@Bean
	public DataSource dataSource() throws SQLException {
		HikariDataSource dataSource = new HikariDataSource(this);
		dataSource.setDataSourceClassName(dataSourceClassName);
		return dataSource;
	}

}
