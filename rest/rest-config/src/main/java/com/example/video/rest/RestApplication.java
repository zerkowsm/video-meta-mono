package com.example.video.rest;

import org.apache.coyote.http11.AbstractHttp11Protocol;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by mjzi on 2017-07-15
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.example.video")
public class RestApplication {

	public static void main(String... args) {
		SpringApplication.run(RestApplication.class, args);
	}

	@Bean
	public TomcatEmbeddedServletContainerFactory containerFactory() {
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
		factory.addConnectorCustomizers((TomcatConnectorCustomizer) connector -> ((AbstractHttp11Protocol) connector.getProtocolHandler()).setMaxSwallowSize(-1));
		return factory;
	}
}