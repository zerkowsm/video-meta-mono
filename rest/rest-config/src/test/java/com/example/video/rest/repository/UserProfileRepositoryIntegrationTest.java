package com.example.video.rest.repository;

import com.example.persistence.model.User;
import com.example.video.repository.UserProfileRepository;
import com.example.video.rest.api.config.integration.IntegrationTest;
import com.example.video.rest.api.config.integration.JpaBasedTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by mjzi on 2017-07-16
 */
@Category(IntegrationTest.class)
public class UserProfileRepositoryIntegrationTest extends JpaBasedTest {

	@Autowired
	UserProfileRepository userProfileRepository;

	@Test
	public void shouldFindUserSuccessfully() {
		//when
		User user = userProfileRepository.findByUsername("user");

		//then
		assertThat(user.getId()).isEqualTo(1L);

		//and
		assertThat(user.getUsername()).isEqualTo("user");

		//and
		assertThat(user.getUserRole().size()).isEqualTo(1);

		//and
		assertThat(user.getUserRole().iterator().next().getRole()).isEqualTo("ROLE_USER");
	}
}