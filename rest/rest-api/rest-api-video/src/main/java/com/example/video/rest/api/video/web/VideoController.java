package com.example.video.rest.api.video.web;

import com.example.video.rest.api.video.service.VideoService;
import com.example.video.rest.api.video.web.dto.VideoDTO;
import com.example.video.rest.api.video.web.dto.VideoDataDTO;
import com.example.video.rest.api.video.web.exception.VideoBusinessException;
import com.example.video.rest.api.web.exception.GlobalBusinessException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.example.video.rest.api.video.web.Mappings.VIDEOS;
import static com.example.video.rest.api.video.web.Mappings.VIDEO_BY_ID;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

/**
 * Created by mjzi on 2017-07-15
 */
@RestController
public class VideoController extends VideoBaseController {

	private final VideoService videoService;

	public VideoController(VideoService videoService) {
		this.videoService = videoService;
	}

	@PostMapping(value = VIDEOS)
	public VideoDTO upload(@RequestParam("file") MultipartFile file) throws IOException, GlobalBusinessException {
		Long videoId = videoService.upload(file);

		VideoDTO videoDTO = new VideoDTO(videoId);
		videoDTO.add(linkTo(VideoController.class).slash(VIDEOS).slash(videoId).withSelfRel());
		return videoDTO;
	}

	@GetMapping(value = VIDEO_BY_ID)
	public VideoDataDTO get(@PathVariable("id") Long videoId) throws VideoBusinessException {
		return new VideoDataDTO(videoService.getVideoFor(videoId));
	}
}
