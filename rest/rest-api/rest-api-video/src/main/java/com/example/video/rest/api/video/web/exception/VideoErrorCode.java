package com.example.video.rest.api.video.web.exception;

/**
 * Created by mjzi on 2017-07-17
 */
public enum VideoErrorCode {

	VIDEO_NOT_FOUND("4V10", "Video not found fot the given id");

	private final String code;

	private final String reason;

	VideoErrorCode(String code, String reason) {
		this.code = code;
		this.reason = reason;
	}

	public String getCode() {
		return code;
	}

	public String getReason() {
		return reason;
	}

}
