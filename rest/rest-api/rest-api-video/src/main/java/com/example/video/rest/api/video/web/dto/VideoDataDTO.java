package com.example.video.rest.api.video.web.dto;

import com.example.persistence.model.Video;
import lombok.Value;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by mjzi on 2017-07-15
 */
@Value
public class VideoDataDTO extends ResourceSupport {
	String duration;
	String size;
	String videoBitRate;
	String videoCodec;
	String audioBitRate;
	String audioCodec;

	public VideoDataDTO(Video video) {
		this.duration = video.getDuration();
		this.size = video.getSize();
		this.videoBitRate = video.getVideoBitRate();
		this.videoCodec = video.getVideoCodec();
		this.audioBitRate = video.getAudioBitRate();
		this.audioCodec = video.getAudioCodec();
	}

	public String getDuration() {
		return duration + " ms";
	}

	public String getSize() {
		return size + " byte";
	}

	public String getVideoBitRate() {
		return videoBitRate + " b/s";
	}

	public String getAudioBitRate() {
		return audioBitRate + " b/s";
	}
}
