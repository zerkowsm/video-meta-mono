package com.example.video.rest.api.video.web.exception;

import com.example.video.rest.api.web.exception.GlobalBusinessException;

/**
 * Created by mjzi on 2017-07-17
 */
public class VideoBusinessException extends GlobalBusinessException {

	public VideoBusinessException(VideoErrorCode errorCode) {
		super(errorCode.getReason(), errorCode.getCode());
	}

}