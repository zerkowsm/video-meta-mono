package com.example.video.rest.api.video.service;

import com.example.persistence.model.Video;
import com.example.video.MetaExtractor;
import com.example.video.repository.VideoRepository;
import com.example.video.rest.api.video.web.exception.VideoBusinessException;
import com.example.video.rest.api.web.exception.GlobalBusinessException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import static com.example.video.rest.api.video.web.exception.VideoErrorCode.VIDEO_NOT_FOUND;

/**
 * Created by mjzi on 2017-07-15
 */
@Service
public class VideoService {

	private final VideoRepository videoRepository;
	private final MetaExtractor metaExtractor;

	public VideoService(VideoRepository videoRepository, MetaExtractor metaExtractor) {
		this.videoRepository = videoRepository;
		this.metaExtractor = metaExtractor;
	}

	public Long upload(MultipartFile file) throws IOException, GlobalBusinessException {
		return videoRepository.save(metaExtractor.getMetaDataFrom(file)).getId();
	}

	public Video getVideoFor(Long videoId) throws VideoBusinessException {
		return videoRepository.findById(videoId).orElseThrow(() -> new VideoBusinessException(VIDEO_NOT_FOUND));
	}
}
