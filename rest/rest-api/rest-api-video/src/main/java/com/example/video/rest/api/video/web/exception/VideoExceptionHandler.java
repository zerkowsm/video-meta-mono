package com.example.video.rest.api.video.web.exception;

import com.example.video.rest.api.video.web.VideoController;
import com.example.video.rest.api.web.exception.GlobalExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * Created by mjzi on 2017-07-17
 */
@ControllerAdvice(assignableTypes = {VideoController.class})
public class VideoExceptionHandler extends GlobalExceptionHandler {
}

