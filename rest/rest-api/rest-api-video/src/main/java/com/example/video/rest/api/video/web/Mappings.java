package com.example.video.rest.api.video.web;

/**
 * Created by mjzi on 2017-07-15
 */
public final class Mappings {

    //TODO: version of api endpoint could be added - skipping that for now
    public static final String VIDEO_API_PREFIX = "/api/video-meta";

    public static final String VIDEOS = "/videos";
    public static final String VIDEO_BY_ID = "/videos/{id}";

    private Mappings() {
    }

}