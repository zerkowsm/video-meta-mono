package com.example.video.rest.api.video.web;

import org.springframework.web.bind.annotation.RequestMapping;

import static com.example.video.rest.api.video.web.Mappings.VIDEO_API_PREFIX;

/**
 * Created by mjzi on 2017-07-15
 */
@RequestMapping(VIDEO_API_PREFIX)
public class VideoBaseController {
}
