package com.example.video.rest.api.video.web.dto;

import lombok.Value;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by mjzi on 2017-07-15
 */
@Value
public class VideoDTO extends ResourceSupport {
	private Long videoId;
}
