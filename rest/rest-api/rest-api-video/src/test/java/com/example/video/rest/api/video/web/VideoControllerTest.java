package com.example.video.rest.api.video.web;

import com.example.persistence.model.Video;
import com.example.video.rest.api.config.mockito.BaseMockitoAwareTest;
import com.example.video.rest.api.video.service.VideoService;
import com.example.video.rest.api.video.web.dto.VideoDataDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static com.example.video.rest.api.video.web.Mappings.*;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class VideoControllerTest extends BaseMockitoAwareTest {

	private MockMvc mockMvc;

	@Mock
	VideoService videoService;

	VideoController videoController;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		videoController = new VideoController(videoService);
		mockMvc = standaloneSetup(videoController).build();
	}

	@Test
	public void shouldReturnMediaInfoResponseSuccessfully() throws Exception {
		//given
		Video video = new Video("1800", "1901822", "7711741", "AVC", "705600", "PCM", "Video.MOV");
		VideoDataDTO videoDataDTO = new VideoDataDTO(video);

		//and
		Long videoId = 1L;

		//when
		doReturn(video).when(videoService).getVideoFor(videoId);

		//then
		mockMvc.perform(get(VIDEO_API_PREFIX + VIDEO_BY_ID, videoId)
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.duration", is(videoDataDTO.getDuration())))
			.andExpect(jsonPath("$.size", is(videoDataDTO.getSize())))
			.andExpect(jsonPath("$.videoBitRate", is(videoDataDTO.getVideoBitRate())))
			.andExpect(jsonPath("$.videoCodec", is(videoDataDTO.getVideoCodec())))
			.andExpect(jsonPath("$.audioBitRate", is(videoDataDTO.getAudioBitRate())))
			.andExpect(jsonPath("$.audioCodec", is(videoDataDTO.getAudioCodec())));
	}

	@Test
	public void shouldUploadVideoFileSuccessfully() throws Exception {
		//given
		String fakeVideoContent = "1234,567,890";
		MockMultipartFile fileToBeUploaded = new MockMultipartFile("file", "Video.MOV", "text/plain", fakeVideoContent.getBytes());

		//and
		Long storedVideoId = 1L;

		//when
		doReturn(storedVideoId).when(videoService).upload(fileToBeUploaded);

		//then
		mockMvc.perform(MockMvcRequestBuilders.fileUpload(VIDEO_API_PREFIX + VIDEOS)
			.file(fileToBeUploaded))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.videoId", is(storedVideoId.intValue())))
			.andExpect(jsonPath("$.links[0].rel", is("self")))
			.andExpect(jsonPath("$.links[0].href", is("http://localhost/api/video-meta/videos/" + storedVideoId)));
	}
}