package com.example.video.rest.api.video.web.exception;

import com.example.video.rest.api.config.junitparams.BaseJUnitParamsAwareTest;
import com.example.video.rest.api.web.dto.GlobalErrorResponse;
import com.example.video.rest.api.web.exception.GlobalBusinessException;
import com.example.video.rest.api.web.exception.GlobalExceptionHandler;
import junitparams.Parameters;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by mjzi on 2017-07-17
 */
public class VideoExceptionHandlerTest extends BaseJUnitParamsAwareTest {

	@Test
	@Parameters(method = "testDataLoad")
	public void shouldReturnAdminErrorResponseProperlyPrepared(VideoErrorCode videoErrorCode) {
		//given
		GlobalExceptionHandler videoExceptionHandler = new VideoExceptionHandler();
		GlobalBusinessException videoBusinessException = new VideoBusinessException(videoErrorCode);
		GlobalErrorResponse errorResponse = new GlobalErrorResponse(videoErrorCode.getCode(), videoErrorCode.getReason());

		//and
		HttpHeaders headers = new HttpHeaders();
		headers.add(CONTENT_TYPE, APPLICATION_JSON_VALUE);
		ResponseEntity adminExpectedErrorResponse = new ResponseEntity<>(errorResponse, headers, HttpStatus.BAD_REQUEST);

		//when then
		assertEquals("Returned error response is different from the expected one", adminExpectedErrorResponse, videoExceptionHandler.handleException(videoBusinessException));
	}

	private Object testDataLoad() {
		return VideoErrorCode.values();
	}


}