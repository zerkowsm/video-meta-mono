package com.example.video.store.fs;

import com.example.persistence.model.Video;
import com.example.video.store.VideoDataStore;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;

/**
 * Created by mjzi on 2017-07-15
 */
@Service(value = "fileSystemDataStore")
public class FileSystemDataStore implements VideoDataStore {

	@Override
	public Video storeVideo(MultipartFile video, Video videoMetaData) throws SQLException, IOException {
		final String videoName = video.getOriginalFilename();

		//TODO: Add property for a file share location
		Files.copy(video.getInputStream(), Paths.get(".").resolve(videoName), StandardCopyOption.REPLACE_EXISTING);
		videoMetaData.setName(videoName);
		return videoMetaData;
	}
}
