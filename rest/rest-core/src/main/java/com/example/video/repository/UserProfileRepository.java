package com.example.video.repository;

import com.example.persistence.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Created by mjzi on 2017-07-16
 */
@RestResource(exported = false)
public interface UserProfileRepository extends JpaRepository<User, Long>, QueryDslPredicateExecutor<User> {

	User findByUsername(String username);
}
