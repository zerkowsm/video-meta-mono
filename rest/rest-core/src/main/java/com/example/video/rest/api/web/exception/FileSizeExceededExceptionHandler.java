package com.example.video.rest.api.web.exception;

import com.example.video.rest.api.web.dto.GlobalErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;

import static com.example.video.rest.api.web.exception.GlobalErrorCode.FILE_SIZE_EXCEEDED;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by mjzi on 2017-07-20
 */
@ControllerAdvice
@Slf4j
public class FileSizeExceededExceptionHandler {

	@Value("${spring.http.multipart.max-file-size:25MB}")
	private String maxFileSize;

	@ExceptionHandler({MultipartException.class, MaxUploadSizeExceededException.class})
	@ResponseStatus(value = HttpStatus.PAYLOAD_TOO_LARGE)
	@ResponseBody
	public ResponseEntity handleMultipartException(MultipartException exception) {

		log.debug("Multipart exception caught {}", exception.getMessage());

		GlobalErrorResponse errorResponse = prepareErrorResponse();

		HttpHeaders headers = new HttpHeaders();
		headers.add(CONTENT_TYPE, APPLICATION_JSON_VALUE);

		return new ResponseEntity<>(errorResponse, headers, BAD_REQUEST);
	}

	GlobalErrorResponse prepareErrorResponse() {
		return new GlobalErrorResponse(FILE_SIZE_EXCEEDED.getCode(), FILE_SIZE_EXCEEDED.getReason() + " - " + maxFileSize);
	}
}