package com.example.video;

import com.abercap.mediainfo.api.MediaInfo;
import com.example.persistence.model.Video;
import com.example.video.rest.api.web.exception.GlobalBusinessException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import static com.example.video.rest.api.web.exception.GlobalErrorCode.FILE_WRONG_FORMAT;
import static com.example.video.rest.api.web.exception.GlobalErrorCode.MISSING_DRIVER;

/**
 * Created by mjzi on 2017-07-15
 */
@Service
@Slf4j
public class MetaExtractor {

	private MediaInfo mediaInfo;

	@PostConstruct
	private void initializeMediaInfoLibrary() {
		try {
			mediaInfo = new MediaInfo();
		} catch (Throwable t) {
			//TODO: Should the whole service start in such case or not? Or maybe extract this class to separated microservice?
			log.error("Error while initialing MediaInfo library {}", t.getMessage());
		}
	}

	public Video getMetaDataFrom(MultipartFile file) throws GlobalBusinessException, IOException {
		if (mediaInfo == null) {
			log.error("MediaInfo library not properly initialized");
			throw new GlobalBusinessException(MISSING_DRIVER.getReason(), MISSING_DRIVER.getCode());
		}

		final String fileName = file.getOriginalFilename();

		//TODO: Add checksum generator so we do not store the same files, add also stream compressor and encryptor
		//TODO: Try to work on stream instead of flushing it to disk (mediaInfo library to be reviewed) but still data store is needed...
		//TODO: Add property for a file share location
		//TODO: Consider catching IOException
		Files.copy(file.getInputStream(), Paths.get(".").resolve(fileName), StandardCopyOption.REPLACE_EXISTING);

		Video video;
		try {
			mediaInfo.open(Paths.get(".").resolve(fileName).toFile());
			video = getVideoMetaData(file, mediaInfo);
		} catch (Throwable t) {
			log.error("Error while parsing file - possibly not a video format", t.getMessage());
			throw new GlobalBusinessException(FILE_WRONG_FORMAT.getReason(), FILE_WRONG_FORMAT.getCode());
		} finally {
			mediaInfo.close();
			//TODO: Consider removing file - not needed any more? Combine that with video store?
		}
		return video;
	}

	private Video getVideoMetaData(MultipartFile file, MediaInfo mediaInfo) {
		return Video.builder()
			.duration(mediaInfo.get(MediaInfo.StreamKind.General, 0, "Duration", MediaInfo.InfoKind.Text))
			.size(mediaInfo.get(MediaInfo.StreamKind.General, 0, "FileSize", MediaInfo.InfoKind.Text))
			.videoBitRate(mediaInfo.get(MediaInfo.StreamKind.Video, 0, "BitRate", MediaInfo.InfoKind.Text))
			.videoCodec(mediaInfo.get(MediaInfo.StreamKind.Video, 0, "Codec", MediaInfo.InfoKind.Text))
			.audioBitRate(mediaInfo.get(MediaInfo.StreamKind.Audio, 0, "BitRate", MediaInfo.InfoKind.Text))
			.audioCodec(mediaInfo.get(MediaInfo.StreamKind.Audio, 0, "Codec", MediaInfo.InfoKind.Text))
			.name(file.getOriginalFilename())
			.build();
	}

}
