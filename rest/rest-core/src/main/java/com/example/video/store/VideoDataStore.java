package com.example.video.store;

import com.example.persistence.model.Video;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.OperationNotSupportedException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by mjzi on 2017-07-15
 */
//TODO: Add DataStoreException
public interface VideoDataStore {
	Video storeVideo(MultipartFile video, Video videoMetaData) throws SQLException, IOException, OperationNotSupportedException;
}
