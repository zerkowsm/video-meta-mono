package com.example.video.rest.jwt.model;

import lombok.Data;

@Data
public class Credentials {
	private String username;
	private String password;
}
