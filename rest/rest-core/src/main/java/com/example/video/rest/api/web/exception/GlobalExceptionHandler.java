package com.example.video.rest.api.web.exception;

import com.example.video.rest.api.web.dto.GlobalErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by mjzi on 2017-07-17
 */
@Slf4j
public class GlobalExceptionHandler {

	@ExceptionHandler(GlobalBusinessException.class)
	public ResponseEntity handleException(GlobalBusinessException exception) {

		log.debug("Global exception caught {}", exception.getErrorCode());

		GlobalErrorResponse errorResponse = prepareErrorResponseFor(exception);

		HttpHeaders headers = new HttpHeaders();
		headers.add(CONTENT_TYPE, APPLICATION_JSON_VALUE);

		return new ResponseEntity<>(errorResponse, headers, BAD_REQUEST);
	}

	GlobalErrorResponse prepareErrorResponseFor(GlobalBusinessException exception) {
		return new GlobalErrorResponse(exception.getErrorCode(), exception.getMessage());
	}
}