package com.example.video.rest.auth.service;

import com.example.persistence.model.UserRole;
import com.example.video.repository.UserProfileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by mjzi on 2017-07-16
 */
@Service("videoUserDetailsService")
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

	@Autowired
	private UserProfileRepository userProfileRepository;

	@Transactional(readOnly = true)
	@Override
	public UserDetails loadUserByUsername(final String username)
		throws UsernameNotFoundException {

		com.example.persistence.model.User user = userProfileRepository.findByUsername(username);
		Collection<GrantedAuthority> authorities =
			buildUserAuthority(user.getUserRole());

		return buildUserForAuthentication(user, authorities);
	}

	private User buildUserForAuthentication(com.example.persistence.model.User user,
											Collection<GrantedAuthority> authorities) {
		return new User(user.getUsername(), user.getPassword(),
			user.isEnabled(), true, true, true, authorities);
	}

	private Collection<GrantedAuthority> buildUserAuthority(Set<UserRole> userRoles) {

		return userRoles.stream()
			.map(userRole -> new SimpleGrantedAuthority(userRole.getRole()))
			.collect(Collectors.toSet());
	}
}
