package com.example.video.rest.api.web.exception;

/**
 * Created by mjzi on 2017-07-17
 */
public enum GlobalErrorCode {

	MISSING_DRIVER("4G10", "Improper MediaInfo library or MediaInfo library not found"),
	FILE_WRONG_FORMAT("4G11", "Wrong file format - not a video"),
	FILE_SIZE_EXCEEDED("4G12", "File size exceeded");

	private final String code;

	private final String reason;

	GlobalErrorCode(String code, String reason) {
		this.code = code;
		this.reason = reason;
	}

	public String getCode() {
		return code;
	}

	public String getReason() {
		return reason;
	}

}
