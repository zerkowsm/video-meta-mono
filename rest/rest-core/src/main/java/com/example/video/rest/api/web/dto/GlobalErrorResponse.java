package com.example.video.rest.api.web.dto;

import lombok.*;

/**
 * Created by mjzi on 2017-07-15
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class GlobalErrorResponse {
	@NonNull private String errorCode;
	@NonNull private String message;

	//TODO: Can be considered as not a good idea from security reasons - adding just as an example, not being filled out
	private String stacktrace;
}
