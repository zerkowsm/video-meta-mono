package com.example.video.repository;

import com.example.persistence.model.VideoDataBlob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RestResource;

/**
 * Created by mjzi on 2017-07-15
 */
@RestResource(exported = false)
public interface VideoDataBlobRepository extends JpaRepository<VideoDataBlob, Long>, QueryDslPredicateExecutor<VideoDataBlob> {
}
