package com.example.video.store.jpa;

import com.example.persistence.model.Video;
import com.example.persistence.model.VideoDataBlob;
import com.example.video.repository.VideoDataBlobRepository;
import com.example.video.store.VideoDataStore;
import org.apache.commons.io.IOUtils;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;

/**
 * Created by mjzi on 2017-07-15
 */
@Service(value = "jpaDataStore")
public class JPADataStore implements VideoDataStore {

	@Autowired
	EntityManager entityManager;

	@Autowired
	VideoDataBlobRepository videoDataBlobRepository;

	public Video storeVideo(MultipartFile video, Video videoMetaData) throws SQLException, IOException {

		VideoDataBlob videoDataBlob = new VideoDataBlob();

		Session session = entityManager.unwrap(Session.class);

		Blob blob = Hibernate.getLobCreator(session).createBlob(new byte[0]);

		OutputStream outputStream = blob.setBinaryStream(1);

		IOUtils.copyLarge(video.getInputStream(), outputStream);

		outputStream.close();

		videoDataBlob.setData(blob);

		videoMetaData.setName(videoDataBlobRepository.save(videoDataBlob).getId().toString());

		return videoMetaData;
	}
}
