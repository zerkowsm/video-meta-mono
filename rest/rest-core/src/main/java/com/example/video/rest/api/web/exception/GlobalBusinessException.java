package com.example.video.rest.api.web.exception;

/**
 * Created by mjzi on 2017-07-17
 */
public class GlobalBusinessException extends Exception {

	private String errorCode;

	public GlobalBusinessException(String errorCode) {
		super();
		this.errorCode = errorCode;
	}

	public GlobalBusinessException(String message, String errorCode) {
		super(message);
		this.errorCode = errorCode;
	}

	public GlobalBusinessException(String message, String errorCode, Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	public String getErrorCode() {
		return errorCode;
	}
}