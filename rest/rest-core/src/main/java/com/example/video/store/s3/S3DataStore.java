package com.example.video.store.s3;

import com.example.persistence.model.Video;
import com.example.video.store.VideoDataStore;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.naming.OperationNotSupportedException;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by mjzi on 2017-07-15
 */
@Service(value = "s3DataStore")
public class S3DataStore implements VideoDataStore {
	@Override
	public Video storeVideo(MultipartFile video, Video videoMetaData) throws SQLException, IOException, OperationNotSupportedException {
		throw new OperationNotSupportedException("Operation not supported yet.");
	}
}
