package com.example.video.rest.auth.provider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

/**
 * Created by mjzi on 2017-07-16
 */
//TODO: add password encoder and salt...
@Component("videoAuthenticationProvider")
public class AuthenticationProvider extends DaoAuthenticationProvider {

	//TODO: add this to properties
	@Value("${demo.auth.login.max-failed-attempts:3}")
	private short maxFailedLoginAttempts;

	@Value("${demo.auth.login.failed-attempt-expiry-seconds:30}")
	private int failedLoginExpireTime;

	@Autowired
	@Qualifier("videoUserDetailsService")
	@Override
	public void setUserDetailsService(UserDetailsService userDetailsService) {
		super.setUserDetailsService(userDetailsService);
	}

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		try {
			Authentication auth = super.authenticate(authentication);
			resetFailAttempts(authentication.getName());
			return auth;
		} catch (BadCredentialsException e) {
			Integer failAttemptCount = updateFailAttempts(authentication.getName());
			if (failAttemptCount != null && failAttemptCount >= maxFailedLoginAttempts) {
				throw new LockedException("User is locked due to multiple fail attempt!");
			} else {
				throw e;
			}
		}
	}

	//TODO: fill this out
	private void resetFailAttempts(String userToken) {
	}

	//TODO: fill this out
	private Integer updateFailAttempts(String userToken) {
		return null;
	}

}