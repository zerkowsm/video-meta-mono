package com.example.video.repository;

import com.example.persistence.model.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Optional;

/**
 * Created by mjzi on 2017-07-15
 */
@RestResource(exported = false)
public interface VideoRepository extends JpaRepository<Video, Long>, QueryDslPredicateExecutor<Video> {
	Optional<Video> findById(Long id);
}
