package com.example.video.rest.api.config.mockito;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by mjzi on 2017-07-17
 */
@RunWith(MockitoJUnitRunner.class)
public abstract class BaseMockitoAwareTest {

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
}
