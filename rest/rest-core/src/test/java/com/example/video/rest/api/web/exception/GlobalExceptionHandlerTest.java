package com.example.video.rest.api.web.exception;

import com.example.video.rest.api.config.junitparams.BaseJUnitParamsAwareTest;
import com.example.video.rest.api.web.dto.GlobalErrorResponse;
import junitparams.Parameters;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * Created by mjzi on 2017-07-17
 */
public class GlobalExceptionHandlerTest extends BaseJUnitParamsAwareTest {

	@Test
	@Parameters(method = "testDataLoad")
	public void shouldReturnGlobalErrorResponseProperlyPrepared(GlobalErrorCode globalErrorCode) {

		//given
		GlobalExceptionHandler globalExceptionHandler = new GlobalExceptionHandler();
		GlobalBusinessException globalBusinessException = new GlobalBusinessException(globalErrorCode.getReason(),globalErrorCode.getCode());
		GlobalErrorResponse errorResponse = new GlobalErrorResponse(globalErrorCode.getCode(), globalErrorCode.getReason());

		//and
		HttpHeaders headers = new HttpHeaders();
		headers.add(CONTENT_TYPE, APPLICATION_JSON_VALUE);
		ResponseEntity expectedGlobalErrorResponse = new ResponseEntity<>(errorResponse, headers, HttpStatus.BAD_REQUEST);

		//when then
		assertEquals("Returned error response is different from the expected one", expectedGlobalErrorResponse, globalExceptionHandler.handleException(globalBusinessException));
	}

	private Object testDataLoad() {
		return GlobalErrorCode.values();
	}

}