package com.example.video.rest.api.config.junitparams;

import junitparams.JUnitParamsRunner;
import org.junit.runner.RunWith;

/**
 * Created by mjzi on 2017-07-17
 */
@RunWith(JUnitParamsRunner.class)
public abstract class BaseJUnitParamsAwareTest {
}
